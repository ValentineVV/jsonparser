//
//  ParsingPlanets.m
//  JSONParser
//
//  Created by Valiantsin Vasiliavitski on 4/10/18.
//  Copyright © 2018 Valiantsin Vasiliavitski. All rights reserved.
//

#import "ParsingPlanets.h"

@implementation ParsingPlanets

+(NSArray<Planet *> *)parsePlanets:(NSData*)data {
    NSError *error = nil;
    NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
    if (error) {
        NSLog(@"%@", error);
    } else {
        
        NSMutableArray<Planet*> *planets = [[NSMutableArray alloc] init];
        NSArray *parsedPlanets = [dictionary valueForKey:@"results"];
        
        for (NSDictionary *planetDic in parsedPlanets) {
            Planet *planet = [[Planet alloc] init];
            
            planet.name = [planetDic valueForKey:@"name"];
            planet.rotationPeriod = [[planetDic valueForKey:@"rotation_period"] integerValue];
            planet.orbitalPeriod = [[planetDic valueForKey:@"orbital_period"] integerValue];
            planet.diameter = [[planetDic valueForKey:@"diameter"] integerValue];
            planet.climate = [planetDic valueForKey:@"climate"];
            planet.gravity = [planetDic valueForKey:@"gravity"];
            planet.terrain = [planetDic valueForKey:@"terrain"];
            planet.surfaceWater = [[planetDic valueForKey:@"surface_water"] integerValue];
            planet.population = [[planetDic valueForKey:@"population"] integerValue];
            planet.residents = [planetDic valueForKey:@"residents"];
            planet.films = [planetDic valueForKey:@"films"];
            planet.created = [self formatDate:[planetDic valueForKey:@"created"]];
            planet.edited = [self formatDate:[planetDic valueForKey:@"edited"]];
            planet.url = [NSURL URLWithString:[planetDic valueForKey:@"url"]];
            
            [planets addObject:planet];
        }
        return planets;
    }
    return nil;
}

+(NSDate*)formatDate:(NSString*)dateString {
    
    NSDateFormatter *formatter = [NSDateFormatter new];
    [formatter setDateFormat:@"yyyy'-'MM'-'dd'T'hh:mm:ss.SZ"];
    return [formatter dateFromString:dateString];
}

@end
