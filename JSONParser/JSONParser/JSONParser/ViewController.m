//
//  ViewController.m
//  JSONParser
//
//  Created by Valiantsin Vasiliavitski on 4/10/18.
//  Copyright © 2018 Valiantsin Vasiliavitski. All rights reserved.
//

#import "ViewController.h"
#import "ParsingPlanets.h"

@interface ViewController () <NSURLSessionDataDelegate>

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSURL * url = [NSURL URLWithString:@"http://swapi.co/api/planets/?page=1"];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config delegate:self delegateQueue:nil];
    NSURLSessionTask *task = [session dataTaskWithRequest:request];
    [task resume];
    // Do any additional setup after loading the view, typically from a nib.
}

-(void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didCompleteWithError:(NSError *)error {
    NSLog(@"%@", error);
}

-(void)URLSession:(NSURLSession *)session dataTask:(NSURLSessionDataTask *)dataTask didReceiveData:(NSData *)data {
    
    NSArray<Planet*> *planets = [ParsingPlanets parsePlanets:data];
    NSLog(@"%ld", [planets count]);
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
