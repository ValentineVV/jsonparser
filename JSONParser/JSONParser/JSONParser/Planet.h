//
//  Planet.h
//  JSONParser
//
//  Created by Valiantsin Vasiliavitski on 4/10/18.
//  Copyright © 2018 Valiantsin Vasiliavitski. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Planet : NSObject

@property (nonatomic, strong) NSString *name;
@property (nonatomic, assign) NSInteger rotationPeriod;
@property (nonatomic, assign) NSInteger orbitalPeriod;
@property (nonatomic, assign) NSInteger diameter;
@property (nonatomic, strong) NSString *climate;
@property (nonatomic, strong) NSString *gravity;
@property (nonatomic, strong) NSString *terrain;
@property (nonatomic, assign) NSInteger surfaceWater;
@property (nonatomic, assign) NSInteger population;
@property (nonatomic, strong) NSArray *residents;
@property (nonatomic, strong) NSArray *films;
@property (nonatomic ,strong) NSDate *created;
@property (nonatomic, strong) NSDate *edited;
@property (nonatomic, strong) NSURL *url;

@end
