//
//  ParsingPlanets.h
//  JSONParser
//
//  Created by Valiantsin Vasiliavitski on 4/10/18.
//  Copyright © 2018 Valiantsin Vasiliavitski. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Planet.h"

@interface ParsingPlanets : NSObject

+(NSArray<Planet*>*)parsePlanets:(NSData *)data;

@end
